---
layout: markdown_page
title: Remote Only
---

## Manifesto

While there sometimes is value in the items on the right, we we have come to value the items on the left more:

1. Hire and work from all over the world instead of from a central location
1. Flexible working hours over set working hours
1. Writing down and recorded knowledge over verbal explanations
1. Written down processes over on-the-job training
1. Public sharing of information over need to know access
1. Every document is open to change by anyone over top down control of documents
1. [Asynchronous communication](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) over synchronous communication
1. The results of work over the hours put in.
1. Formal communication channels over informal communication channels

## Practical tips

1. People don't have to say when they are working.
1. Working long hours or weekends in not encouraged nor celebrated.
1. Use screenshots in an issue tracker instead of a whiteboard, ensuring that everyone at any time can follow the thought process.
1. Company encourages non-work related communication (talking about private life on a team call),
1. Encourage group video calls for [bonding](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
1. Encourage [video calls](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) between people (10 as part of onboarding)
1. Periodic summits with the whole company to get to know each other in an informal setting.
1. Encourage [saying thanks and teamwork](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
1. Assign new hires a buddy so they have someone to reach out to.
1. Allow everyone in the company to view and edit every document.
1. Every document is always in draft, don't wait with sharing it until it is done.
1. Encourage people to write information down.

## What is it not

1. There is no main office or headquarter with multiple people _"The only way to not have people in a satellite office is not to have a main office."_
1. You do work together and communicate intensely, remote doesn't mean working independent of each other.
1. Offshoring of work, you hire around the world.
1. It is not a management paradigm, it is still a normal hierarchical organization, however there is a focus on output instead of input.
1. It is not a substitute for human interaction, please still need to collaborate, have conversations, and feel part of a team.

## How it changes the organization

1. Knowledge is written down instead of passed verbally
1. More asynchronous communication
1. Shorter and fewer meetings
1. More transparency within and outside the organization
1. Everything is public by default
1. More official communication, less informal
1. More recorded materials means less interruptions and less on-the-job training

## Advantages for employees

1. More flexibility in your daily life (kids, parents, friends, groceries, sports, deliveries)
1. No [commuting time or stress](http://www.scientificamerican.com/article/commuting-takes-its-toll/)
1. Reduce the [interruption stress](https://www.washingtonpost.com/posteverything/wp/2014/12/30/google-got-it-wrong-the-open-office-trend-is-destroying-the-workplace/)
1. Traveling to other places without taking vacation (family, fun, etc.)
1. Free to move to other places

## Advantages for organizations

1. Hire great people irrespective of where they live
1. More effective employees since they have less distractions
1. More loyal employees
1. Save on office costs
1. Save on compensation due to hiring in lower cost regions
1. Selects for self starting people
1. Makes it easy to grow a company quickly
1. Encourages a focus on results, less meetings, more output.

## Advantages for the world

1. Reduce environmental impact due to no [commuting](http://www.scientificamerican.com/article/commuting-takes-its-toll/)
1. Reduce environmental impact due to less office space
1. Reduce inequality due to bringing better paying jobs to lower cost regions

## Disadvantages

1. Scares investors
1. Scares some partners
1. Scares some customers
1. Scares some potential employees, mostly senior non-technical hires
1. Onboarding is harder, first month feels lonely

## Why is this possible now

1. Fast internet everywhere - 100MB/s+ cable, 5Ghz Wifi, 4G cellular
1. Video calls - Google Hangouts, Bluejeans
1. Messaging apps - Slack, Mattermost
1. Issue trackers - Trello, GitHub issues, GitLab issues
1. Suggestions - GitHub Pull Requests, GitLab Merge Requests
1. Static websites - GitHub Pages, GitLab Pages
1. English proficiency - More people are learning English

## Remote only companies

1. [Buffer](https://buffer.com), see their posts about going [remote-only](https://open.buffer.com/no-office/), [the benefits](https://open.buffer.com/distributed-team-benefits/), and how they [make it work](https://open.buffer.com/buffer-distributed-team-how-we-work/).
1. [GitLab](https://about.gitlab.com), this website is hosted by GitLab, everyone is welcome to contribute to [this page](https://gitlab.com/gitlab-com/www-remoteonly-org/blob/master/source/index.html.md), also see [our handbook](https://about.gitlab.com/handbook/) for remote working practices.
1. [Groove](https://www.groovehq.com/), see their blog on being a [remote team](https://www.groovehq.com/blog/being-a-remote-team).

Know of more? Please [contribute to this page](https://gitlab.com/gitlab-com/www-remoteonly-org/)

**Remote first companies**

- [Basecamp](https://basecamp.com/), authors of [Remote](https://37signals.com/remote)
- [Wordpress](https://wordpress.com/), authors of [The year without pants](https://www.amazon.com/Year-Without-Pants-WordPress-com-Future/dp/1118660633)

**Organizations promoting remote work**

- [Remote Year](http://www.remoteyear.com/)

**Job boards aimed at remote workers**

See a collection with reviews of [25 sites](http://skillcrush.com/2014/10/10/sites-finding-remote-work/), some of which are listed below:

1. [We Work Remotely](https://weworkremotely.com)
1. [Remote OK](https://remoteok.io)
1. [Jobspresso](https://jobspresso.co/)
1. [Working Nomads](http://www.workingnomads.co/jobs)
1. [PowerToFly](https://powertofly.com)
1. [Remote jobs on Angellist](https://angel.co/job-collections/remote)

## References

1. [After Growing to 50 People, We’re Ditching the Office Completely](https://open.buffer.com/no-office/)
1. [Remote working tips by Groove](https://www.groovehq.com/blog/remote-work-tips)
1. [Remote manifesto by GitLab](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
1. [Being tired isn’t a badge of honor](https://m.signalvnoise.com/being-tired-isn-t-a-badge-of-honor-fa6d4c8cff4e)
1. [The Day They Invented Offices](https://shift.infinite.red/a-hypothetical-conversation-with-a-real-estate-developer-in-a-world-without-offices-53cd7be0942#.pufgl7l3a)
1. [Introverts at Work: Designing Spaces for People Who Hate Open-Plan Offices](http://www.bloomberg.com/news/articles/2014-06-16/open-plan-offices-for-people-who-hate-open-plan-offices)
1. [That remote work think piece has some glaring omissions (a rant)](http://www.catehuston.com/blog/2016/04/07/that-remote-work-think-piece-has-some-glaring-omissions/)
1. [How I manage 40 people remotely](http://ryancarson.com/post/24884883426/how-i-manage-40-people-remotely)
1. [Guidelines for Effective Collaboration](https://github.com/ride/collaboration-guides)
1. [The Ultimate Guide to Remote Standups](http://blog.idonethis.com/ultimate-guide-remote-standups/)
1. [How Do You Manage Global Virtual Teams?](https://en.wikibooks.org/wiki/Managing_Groups_and_Teams/How_Do_You_Manage_Global_Virtual_Teams%3F)
1. [Getting Virtual Teams Right](https://hbr.org/2014/12/getting-virtual-teams-right)
